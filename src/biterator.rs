use crate::{U1,U4,CustomInt};
use paste::paste;
use std::io::Read;

macro_rules! sub_iter_impl {
    ($name:ident, $method:ident, $plural:ident, $type:ty $(, $conversion:expr )? ) => {
        paste! {
            pub struct $name<'a, R>(&'a mut Biterator<R>);

            impl<R> Biterator<R> {
                pub fn $plural(&mut self) -> $name<'_, R> {
                    $name::new(self)
                }

                pub fn [< $method _align >](&mut self) {
                    self.align_to_bits::<{ <$type as custom_int::Bits>::BITS as u8 }>()
                }

                pub const fn [< is_ $method _aligned >](&self) -> bool {
                    self.is_aligned_to_bits::<{ <$type as custom_int::Bits>::BITS as u8 }>()
                }
            }

            impl<R: Read> Biterator<R> {
                pub fn [< peek_ $method >](&mut self) -> Option<$type> {
                    self.peek_bits::<{ <$type as custom_int::Bits>::BITS as u8 }>() $( .map($conversion) )?
                }

                pub fn [< next_ $method >](&mut self) -> Option<$type> {
                    self.next_bits::<{ <$type as custom_int::Bits>::BITS as u8 }>() $( .map($conversion) )?
                }

                pub fn [< next_ $method _if >](&mut self, predicate: impl FnOnce($type) -> bool) -> Option<$type> {
                    $( let predicate = |next: CustomInt<_, { <$type as custom_int::Bits>::BITS as u8 }>| predicate($conversion(next)); )?
                    self.next_bits_if(predicate) $( .map($conversion) )?
                }

                pub fn [< next_ $method _if_eq >](&mut self, expected: $type) -> Option<$type> {
                    $( let expected = $conversion(expected); )?
                    self.next_bits_if_eq::<{ <$type as custom_int::Bits>::BITS as u8 }>(expected) $( .map($conversion) )?
                }
            }

            impl<'a, R> $name<'a, R> {
                pub(crate) fn new(biterator: &'a mut Biterator<R>) -> Self {
                    Self(biterator)
                }

                pub fn align(&mut self) {
                    self.0.[< $method _align >]()
                }

                pub const fn is_aligned(&self) -> bool {
                    self.0.[< is_ $method _aligned >]()
                }
            }

            impl<'a, R: Read> $name<'a, R> {
                pub fn peek(&mut self) -> Option<$type> {
                    self.0.[< peek_ $method >]()
                }

                pub fn next_if(&mut self, predicate: impl FnOnce($type) -> bool) -> Option<$type> {
                    self.0.[< next_ $method _if >](predicate)
                }

                pub fn next_if_eq(&mut self, expected: $type) -> Option<$type> {
                    self.0.[< next_ $method _if_eq >](expected)
                }
            }

            impl<R: Read> Iterator for $name<'_, R> {
                type Item = $type;

                fn next(&mut self) -> Option<$type> {
                    self.0.[< next_ $method >]()
                }
            }
        }
    };
}

macro_rules! sub_iter {
    ($name:ident, $method:ident / $plural:ident, $type:ty) => {
        sub_iter_impl!($name, $method, $plural, $type);
    };
    ($name:ident, $method:ident / $plural:ident, native $type:ty) => {
        sub_iter_impl!($name, $method, $plural, $type, Into::into);
    };
}

sub_iter!(Bits, bit / bits, U1);
sub_iter!(Nibbles, nibble / nibbles, U4);
sub_iter!(Bytes, byte / bytes, native u8);

#[derive(Debug)]
pub struct Biterator<R> {
    inner: R,
    next: [Option<Option<u8>>; 2],
    consumed_bits: u8,
    consumed_bytes: usize,
}

impl<R> Biterator<R> {
    pub const fn new(inner: R) -> Self {
        Self {
            inner,
            next: [None; 2],
            consumed_bits: 0,
            consumed_bytes: 0,
        }
    }

    /// Returns the number of complete bytes read so far
    pub const fn consumed_bytes(&self) -> usize {
        self.consumed_bytes
    }

    const fn consumed_bits(&self) -> u8 {
        self.consumed_bits
    }

    const fn remaining_bits(&self) -> u8 {
        8 - self.consumed_bits
    }

    pub const fn is_aligned_to_bits<const BITS: u8>(&self) -> bool {
        (self.remaining_bits() % BITS) == 0
    }

    /// NB: This advances without checking whether there will be data at the new bit.
    fn advance_by_bits(&mut self, bits: u8) {
        // remaining_bits must be at least 1
        if self.remaining_bits() > bits {
            self.consumed_bits += bits;
        } else {
            let consumed_bits = self.consumed_bits() + bits;
            self.consumed_bits = consumed_bits % 8;

            let shift_bytes = usize::from(consumed_bits / 8);
            self.consumed_bytes += shift_bytes;

            for i in 0..self.next.len() {
                self.next[i] = *self.next.get(i + shift_bytes).unwrap_or(&None);
            }
        }
    }

    pub fn align_to_bits<const BITS: u8>(&mut self) {
        if BITS > 1 {
            let to_next_multiple = BITS - (self.consumed_bits() % BITS);
            self.advance_by_bits(to_next_multiple);
        }
    }
}

impl<R: Read> Biterator<R> {
    // fn fill_next(&mut self) {
    //     if self.next.is_none() {
    //         self.next = self.inner.next();
    //     }
    // }

    pub fn peek_bits<const BITS: u8>(&mut self) -> Option<CustomInt<u8, BITS>> {
        // TODO: switch to buffer
        let mut bytes = self.inner.by_ref().bytes();

        let mut bits = BITS;
        let mut result = 0;
        for i in 0..self.next.len() {
            if bits == 0 {
                break;
            }

            let byte = if let Some(byte) =
                self.next[i].get_or_insert_with(|| bytes.next().map(Result::unwrap))
            {
                *byte
            } else {
                return None;
            };

            let consumed_bits = if i == 0 { self.consumed_bits } else { 0 };
            let remaining_bits = 8 - consumed_bits;
            let width = bits.min(remaining_bits);
            let mask = 0xFF >> (8 - width);

            result |= ((byte >> (remaining_bits - width)) & mask) << (bits - width);

            bits -= width;
        }

        Some(CustomInt::<u8, BITS>::new(result))
    }

    pub fn next_bits<const BITS: u8>(&mut self) -> Option<CustomInt<u8, BITS>> {
        let bits = self.peek_bits();

        if bits.is_some() {
            self.advance_by_bits(BITS);
        }

        bits
    }

    pub fn next_bits_if<const BITS: u8>(
        &mut self,
        predicate: impl FnOnce(CustomInt<u8, BITS>) -> bool,
    ) -> Option<CustomInt<u8, BITS>> {
        match self.peek_bits() {
            Some(bits) if predicate(bits) => self.next_bits(),
            _ => None,
        }
    }

    pub fn next_bits_if_eq<const BITS: u8>(
        &mut self,
        expected: CustomInt<u8, BITS>,
    ) -> Option<CustomInt<u8, BITS>> {
        self.next_bits_if(|next| next == expected)
    }
}

// macro_rules! impl_passthrough {
//     (def args ( self, $($arg:ident : $arg_ty:ty),* $(,)? )) => ( (self, $($arg : $arg_ty),*) );
//     (def args ( &self, $($arg:ident : $arg_ty:ty),* $(,)? )) => ( (&self, $($arg : $arg_ty),*) );
//     (def args ( &mut self, $($arg:ident : $arg_ty:ty),* $(,)? )) => ( (&mut self, $($arg : $arg_ty),*) );
//     (body ( $(&$(mut)?)? $self:ident, $($arg:ident : $arg_ty:ty),* $(,)? )) => {
//         $self.inner.$method($($arg),* )
//     };
//     ($( fn $method:ident $args:tt -> $return:ty; )+) => {
//         $(
//         	fn $method $args -> $return {
//             	impl_passthrough!(body $args)
//             }
//         )+
//     }
// }

#[cfg(test)]
mod test {
    use super::*;

    fn bytes_to_biterator(bytes: &[u8]) -> Biterator<&[u8]> {
        Biterator::new(bytes)
    }

    #[test]
    fn consumed_bytes() {
        let mut biterator = bytes_to_biterator(&[0, 0, 0]);

        assert_eq!(0, biterator.consumed_bytes());
        biterator.next_nibble();
        assert_eq!(0, biterator.consumed_bytes());
        biterator.next_byte();
        assert_eq!(1, biterator.consumed_bytes());
        biterator.next_nibble();
        assert_eq!(2, biterator.consumed_bytes());
    }

    #[test]
    fn bits_keep_order() {
        let mut biterator = bytes_to_biterator(&[0b10110111]);

        assert_eq!(Some(0b10110111), biterator.peek_byte());
        assert_eq!(Some(U4::new(0b00001011)), biterator.peek_nibble());
    }

    #[test]
    fn next_byte() {
        let mut biterator = bytes_to_biterator(&[1, 2, 3]);

        assert_eq!(Some(1), biterator.next_byte());
        assert_eq!(Some(2), biterator.next_byte());
        assert_eq!(Some(3), biterator.next_byte());
        assert_eq!(None, biterator.next_byte());
    }

    #[test]
    fn peek_byte() {
        let mut biterator = bytes_to_biterator(&[1, 2, 3]);

        assert_eq!(Some(1), biterator.peek_byte());
        assert_eq!(Some(1), biterator.peek_byte());
        assert_eq!(Some(1), biterator.next_byte());
        assert_eq!(Some(2), biterator.peek_byte());
        assert_eq!(Some(2), biterator.next_byte());
    }

    #[test]
    fn bit_next() {
        let mut biterator = bytes_to_biterator(&[0b10100000, 0xFF]);
        let mut bits = biterator.bits();

        assert_eq!(Some(U1::new(1)), bits.next());
        assert_eq!(Some(U1::new(0)), bits.next());
        assert_eq!(Some(U1::new(1)), bits.next());

        for _ in 0..5 {
            assert_eq!(Some(U1::new(0)), bits.next());
        }

        for _ in 0..8 {
            assert_eq!(Some(U1::new(1)), bits.next());
        }

        assert_eq!(None, bits.next());
    }

    #[test]
    fn bit_peek_and_next() {
        let mut biterator = bytes_to_biterator(&[0b10101010]);
        let mut bits = biterator.bits();

        assert_eq!(Some(U1::new(1)), bits.peek());
        assert_eq!(Some(U1::new(1)), bits.peek());
        assert_eq!(Some(U1::new(1)), bits.next());

        assert_eq!(Some(U1::new(0)), bits.peek());
        assert_eq!(Some(U1::new(0)), bits.next());
    }

    // TODO: rename
    #[test]
    fn test() {
        let mut biterator = bytes_to_biterator(&[0, 1, 2, 3]);
        let mut bytes = biterator.bytes();

        let mut i = 0;
        while let Some(byte) = bytes.peek() {
            assert_eq!(Some(byte), bytes.next());
            assert_eq!(byte, i);
            i += 1;
        }
    }

    #[test]
    fn test_2() {
        let mut biterator = bytes_to_biterator(&[0, 1, 2, 3]);

        let mut i = 0;
        while let Some(byte) = biterator.bytes().peek() {
            assert_eq!(Some(byte), biterator.next_byte());
            assert_eq!(byte, i);
            i += 1;
        }
    }

    #[test]
    fn unaligned() {
        let mut biterator = bytes_to_biterator(&[0b11000100, 0b00000100]);

        assert_eq!(Some(U1::new(1)), biterator.next_bit());
        assert_eq!(Some(U4::new(0b1000)), biterator.next_nibble());
        assert_eq!(Some(0x80), biterator.next_byte());
        assert_eq!(Some(CustomInt::<u8, 3>::new(0b100)), biterator.next_bits());
        assert_eq!(None, biterator.next_bit());
    }

    #[test]
    fn next_if() {
        type U7 = CustomInt<u8,7>;
        let mut biterator = bytes_to_biterator(&[0b10101010]);

        assert_eq!(Some(U1::new(1)), biterator.next_bit_if_eq(U1::new(1)));
        assert_eq!(None, biterator.next_bit_if_eq(U1::new(1)));
        assert_eq!(
            Some(U7::new(0x2A)),
            biterator.next_bits_if(|bits| bits > U7::new(0x08))
        );
        assert_eq!(None, biterator.next_bit_if(|_| true));
    }

    #[test]
    fn peek_bit_keeps_byte_alignment() {
        let mut biterator = bytes_to_biterator(&[0xFF]);

        assert_eq!(Some(U1::new(1)), biterator.peek_bit());
        assert_eq!(Some(0xFF), biterator.next_byte());
        assert_eq!(None, biterator.next_byte());
    }

    #[test]
    fn bit_align() {
        let mut biterator = bytes_to_biterator(&[0b0100_0000]);

        assert!(biterator.bits().is_aligned());
        assert_eq!(Some(U1::new(0)), biterator.next_bit());
        assert!(biterator.is_bit_aligned());
        biterator.bit_align();
        assert!(biterator.is_bit_aligned());
        assert_eq!(Some(U1::new(1)), biterator.next_bit());
        assert!(biterator.is_bit_aligned());
    }

    #[test]
    fn nibble_align() {
        let mut biterator = bytes_to_biterator(&[0x08, 0x10]);

        assert!(biterator.nibbles().is_aligned());
        biterator.next_bit();
        assert!(!biterator.is_nibble_aligned());
        assert_eq!(Some(U4::new(1)), biterator.next_nibble());
        assert!(!biterator.is_nibble_aligned());
        biterator.nibble_align();
        assert!(biterator.is_nibble_aligned());
        assert_eq!(Some(U4::new(1)), biterator.next_nibble());
        assert!(biterator.is_nibble_aligned());
    }

    #[test]
    fn byte_align() {
        let mut biterator = bytes_to_biterator(&[0x00, 0x80, 0x01]);

        assert!(biterator.bytes().is_aligned());
        biterator.next_bit();
        assert!(!biterator.is_byte_aligned());
        assert_eq!(Some(1), biterator.next_byte());
        assert!(!biterator.is_byte_aligned());
        biterator.byte_align();
        assert!(biterator.is_byte_aligned());
        assert_eq!(Some(1), biterator.next_byte());
        assert!(biterator.is_byte_aligned());
    }

    #[test]
    fn empty_align() {
        let mut biterator = bytes_to_biterator(&[]);

        assert!(biterator.is_byte_aligned());
        biterator.byte_align();
        assert!(biterator.is_byte_aligned());
    }
}
