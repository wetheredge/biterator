pub mod biterator;

pub use biterator::Biterator;
pub use custom_int;
pub use custom_int::CustomInt;

pub type U1 = CustomInt<u8, 1>;
pub type U4 = CustomInt<u8, 4>;
